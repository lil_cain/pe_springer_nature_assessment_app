# -*- coding: utf-8 -*-
import unittest
from helloworld_springer_app import config
from test import test_support
from mock import mock_open, patch


from sys import version_info
if version_info.major == 2:
    import __builtin__ as builtins  # pylint:disable=import-error
else:
    import builtins  # pylint:disable=import-error


configuration_file_content = """
date_endpoint: "http://test.file.com/"
timeout: 1
"""

class ConfigurationTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.env = test_support.EnvironmentVarGuard()
        cls.env.set('HELLOWORLD_SPRINGER_APP_TIMEOUT', "5")
        cls.env.set('HELLOWORLD_SPRINGER_APP_DATE_ENDPOINT', "http://test.env.com/")

    def tearDown(self):
        config.configuration = {}

    def test_read_configuration_both_file_and_env_defined(self):
        with self.env, patch('os.path.isfile', return_value=True), \
                patch.object(builtins, 'open', \
                mock_open(read_data=configuration_file_content)):
            
            config.load_config()
            self.assertEqual(config.configuration["timeout"], "5")
            self.assertEqual(config.configuration["date_endpoint"], "http://test.env.com/")

    def test_read_configuration_only_file_defined(self):
        local_env = test_support.EnvironmentVarGuard()
        local_env.unset('HELLOWORLD_SPRINGER_APP_TIMEOUT')
        local_env.unset('HELLOWORLD_SPRINGER_APP_DATE_ENDPOINT')
        with local_env, patch('os.path.isfile', return_value=True), \
                patch.object(builtins, 'open', \
                mock_open(read_data=configuration_file_content)):

            config.load_config()
            self.assertEqual(config.configuration["timeout"], 1)
            self.assertEqual(config.configuration["date_endpoint"], "http://test.file.com/")

    def test_read_configuration_file_empty(self):
        local_env = test_support.EnvironmentVarGuard()
        local_env.unset('HELLOWORLD_SPRINGER_APP_TIMEOUT')
        local_env.unset('HELLOWORLD_SPRINGER_APP_DATE_ENDPOINT')
        with local_env, patch.object(builtins, 'open', mock_open(read_data="")):
            config.load_config()
            self.assertNotIn("timeout", config.configuration)
            self.assertNotIn("date_endpoint", config.configuration)
