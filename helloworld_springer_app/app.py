# -*- coding: utf-8 -*-

import cherrypy
import requests
import config
from jinja2 import Environment, PackageLoader

#loads the templates
env = Environment(loader=PackageLoader('helloworld_springer_app', 'templates'))

def get_date(date_endpoint, timeout):
    """
    Performs a HTTP GET call to an external date service
    """
    s = requests.Session()
    date = s.get(date_endpoint, timeout=timeout)
    return date

class HelloWorldWebService(object):
    """
    Implements a GET method for a REST service
    It performs a call to an external service to get the current date
    """

    exposed = True

    @cherrypy.tools.accept(media='text/plain')
    def GET(self):
    	date = get_date(config.configuration["date_endpoint"], float(config.configuration["timeout"]))
    	tmpl = env.get_template('index.html.j2')
        return tmpl.render(date=date.text)

class HelloWorldReloadConfig(object):
    """
    Implements a POST method for a REST service
    It requires no parameters and issues an internal call for reloading the configuration
    """  
    exposed = True

    @cherrypy.tools.accept(media='text/plain')
    def POST(self):
        s = requests.Session()
        config.reload_config()
        print "Configuration reloaded"
        print config.configuration

def error_page_404(status, message, traceback, version):
    return "404 Error!"

def main():

    config.load_config()

    helloworld_conf = {
         '/': {
             'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
             'tools.response_headers.on': True,
             'tools.response_headers.headers': [('Content-Type', 'text/html')],
         }
     }

    reload_conf = {

         '/': {
             'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
             'tools.response_headers.on': True,
             'tools.response_headers.headers': [('Content-Type', 'text/html')],
         }
     }

    #sets the default error page
    cherrypy.config.update({'error_page.404': error_page_404})

    #mounts the two application endpoint
    cherrypy.tree.mount(HelloWorldWebService(), '/', helloworld_conf)
    cherrypy.tree.mount(HelloWorldReloadConfig(), '/reload', reload_conf)
