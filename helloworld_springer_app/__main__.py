import app
import cherrypy

def main():
    app.main()
    cherrypy.engine.start()
    cherrypy.engine.block()

if __name__ == "__main__":
    main()
